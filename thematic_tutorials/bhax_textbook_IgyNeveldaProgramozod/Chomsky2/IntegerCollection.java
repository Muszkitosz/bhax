public class IntegerCollection {
    private int size;
    private int counter;
    private int[] array;
    private boolean checkIfSorted;

    public IntegerCollection(int s){
        size=s;
	counter=0;
        array=new int[size];
        checkIfSorted=false;
    }

    public void addToCollection(int member){
        if(counter>=size)
            throw new IllegalArgumentException("The collection is full");
        array[counter++]=member;
        checkIfSorted=false;
    }

    public void addToCollection(int[] inputarray) {
        for (int member : inputarray)
            addToCollection(member);
    }

    public void sort(){
        int i=this.size-1;
        while(i>=0){
            for(int j=0;j<i;j++){
                if(this.array[j+1]<this.array[j]){
                    int a=this.array[j+1];
                    this.array[j+1]=this.array[j];
                    this.array[j]=a;
                }
            }
            i--;
        }
        checkIfSorted=true;
    }

    public boolean contains(int member) {
        if (!checkIfSorted)
            sort();
        if (checkIfSorted) {
            int bot = 0;
            int top = this.size;
            while (bot < top) {
                int mid = (bot + top) / 2;
                if (this.array[mid] == member)
                    return true;
                else if (this.array[mid] > member)
                    top = mid - 1;
                else
                    bot = mid + 1;
            }

        }
        return false;
    }


    public void printCollection(){
        String a="";
        for (int i=0;i<this.size;i++) {
            a+=array[i];
            if(i<size-1)
                a+=", ";
            else
                a+=".";
        }
        System.out.println(a);
    }
}
