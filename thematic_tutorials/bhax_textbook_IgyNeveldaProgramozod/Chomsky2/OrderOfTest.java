import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class OrderOfTest {

    @ParameterizedTest
    @MethodSource("dataToBeSorted")
    public void testOrderShouldReturnExpectedListWhenCollectionIsPassed(Collection<Integer> input, List<Integer> expectedOutput) {
        // Given as parameters

        // When
        List<Integer> actualOutput1 = compareCollectionsAsList(input);
        List<Integer> actualOutput2 = compareCollectionsAsStream(input);

        // Then
        assertThat(actualOutput1, equalTo(expectedOutput));
        assertThat(actualOutput2, equalTo(expectedOutput));
    }

    public static Stream<Arguments>dataToBeSorted() {
        return Stream.of(
                Arguments.of(Collections.emptySet(), Collections.emptyList()),
                Arguments.of(Set.of(1), List.of(1)),
                Arguments.of(Set.of(2,1), List.of(1,2))
        );
    }

    private <T extends Comparable<T>> List<T> compareCollectionsAsList(Collection<T> c){
        List list_c = new ArrayList(c);
        Collections.sort(list_c);
        return list_c;
    }

    private <T extends Comparable<T>> List<T> compareCollectionsAsStream(Collection<T> c){
        return c.stream().sorted().collect(Collectors.toList());

    }
}
