import java.io.*;
import java.util.Scanner;

public class StreamEncoder implements AutoCloseable {

	private static final byte[] NEW_LINE = "\n".getBytes();
	
	private Scanner inputScanner;
	private OutputStream outputStream;
	private Encoder encoder;

	public StreamEncoder(String fileName, int offset) {
		this.inputScanner = new Scanner(System.in);
		try {
			this.outputStream = new FileOutputStream(fileName);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		this.encoder = new CaesarCoder(offset);
	}

	public void handleInputs() throws IOException {
		String line;
		do {
			line = inputScanner.nextLine();
			String encodedLine = encoder.encode(line);
			outputStream.write(encodedLine.getBytes());
			outputStream.write(NEW_LINE);
		} while (!"exit".equals(line));
	}

	@Override
	public void close() throws IOException {
		inputScanner.close();
		outputStream.close();
	}

}

