#include <iostream>
#include <boost/filesystem.hpp>

int main(int argc, char* argv[])
{
    if (argc < 2)
    {
      std::cout << "Usage: ./src file/directory_path"<<std::endl;;
      return 1;
    }

    boost::filesystem::path p (argv[1]);

    int count=0;

    std::string ext ( ".java" );

    if (!boost::filesystem::exists(p))
      std::cout<<"Type in a valid file/directory path."<<std::endl;

    else
    {

      for(boost::filesystem::recursive_directory_iterator i=boost::filesystem::recursive_directory_iterator (p); i!=boost::filesystem::recursive_directory_iterator(); i++)
      {
                
        if (!ext.compare(boost::filesystem::extension(*i)))
        {
          count++;
          std::cout<<*i<<std::endl;
        }
      }

      std::cout<<std::endl<<"Total number of classes: "<<count<<std::endl;
    }
}

