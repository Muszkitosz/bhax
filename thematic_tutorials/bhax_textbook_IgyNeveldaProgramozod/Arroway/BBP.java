import java.lang.Math;

public class BBP {

    double s1;
    double s4;
    double s5;
    double s6;
    double pi;
    String codeTable="0123456789ABCDEF";
    String hex="";
    double index;

    public BBP(int digit) {
        s1=algorithm(digit, 1);
        s4=algorithm(digit, 4);
        s5=algorithm(digit, 5);
        s6=algorithm(digit, 6);
        pi=(4*s1-2*s4-s5-s6)%1;
	if (pi<0) {
		pi=pi+1;	
	}
	System.out.println(pi);

        while (pi!=0) {
            index=pi*16-(pi*16)%1;
            hex=hex+codeTable.charAt((int)index);
            pi=(pi*16)%1;
        }

        System.out.println(hex);
    }

    public double algorithm(int digit, int j) {
        double si=0.0;
        for (int k=0;k<=digit;k++) {
            si+=(double)modulo(digit-k,8*k+j)/(double)(8*k+j);
        }
        for (int k=digit+1;k<=2*digit;k++) {
            si+=Math.pow(16,digit-k)/(double)(8*k+j);
        }
        
        si=si%1;
        return si;
    }

    public long modulo(int n, int k) {
        int t=1; //2^0=1
        while (t<=n) {
            t=t*2;
        }
        long r=1;
        int b=16;
        while (true) {
            if (n>=t) {
                r=(b*r)%k;
                n=n-t;
            }
            t=t/2;
            if (t>=1) {
                r=(r*r)%k;
            }
            if (t<1) {
                break;
            }
        }

        return r;
    }

    public static void main(String[] args) {
	System.out.print("How many digits? ");
	String input = System.console().readLine();
	int digits = Integer.parseInt(input);
        new BBP(digits);
    }
}

