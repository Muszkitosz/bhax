#include <stdio.h>
#include <stdbool.h>

int main()
{
    printf("Adjon meg egy számot: ");
    int x;
    int vegtelen=10;
    int szamlalo=0;
    scanf("%d",&x);
    for (x;vegtelen>0;vegtelen++)
        {
            x<<=1;
            vegtelen--;
	    szamlalo++;
		if(x==0)
			{
				break; 		//Arra az esetre, ha az int értéke túlfutna.
			}	
        }
    printf("%d\n",szamlalo);			//Kiiratjuk az int értékét.
}
