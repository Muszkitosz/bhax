#include <stdio.h>
#include <stdlib.h>
#include <curses.h>
#include <unistd.h>
#include <math.h>

void CurrentPos( int* xcoord, int* ycoord, int x, int y, int width, int height )
{
    *xcoord=abs(width-(x%(width*2)));
    *ycoord=abs(height-(y%(height*2)));
    return;
}

int main ()
{
    WINDOW *ablak;
    ablak = initscr ();
    int x = 0;
    int y = 0;
    int width;
    int height;
    int xcoord;
    int ycoord;
    for (;;)
    {
        getmaxyx ( ablak, height, width );
        CurrentPos ( &xcoord, &ycoord, x, y, width, height );
        x+=1;
        y+=1;
        mvprintw ( ycoord, xcoord, "O" );
        refresh ();
        usleep ( 100000 );
        clear();
    }
    return 0;
}