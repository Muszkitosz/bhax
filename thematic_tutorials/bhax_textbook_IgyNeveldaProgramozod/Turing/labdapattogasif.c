#include <stdio.h>
#include <curses.h>
#include <unistd.h>

int
main ( void )
{
    WINDOW *ablak;
    ablak = initscr ();

    int x = 0;
    int y = 0;

    int xnovelo = 1;
    int ynovelo = 1;

    int mx;
    int my;

    for ( ;; ) {

        getmaxyx ( ablak, my , mx );

        mvprintw ( y, x, "O" );

        refresh ();
        usleep ( 100000 );

        x = x + xnovelo;
        y = y + ynovelo;

        if ( x>=mx-1 ) { // elerte-e a jobb oldalt?
            xnovelo = xnovelo * -1;
        }
        if ( x<=0 ) { // elerte-e a bal oldalt?
            xnovelo = xnovelo * -1;
        }
        if ( y<=0 ) { // elerte-e a tetejet?
            ynovelo = ynovelo * -1;
        }
        if ( y>=my-1 ) { // elerte-e a aljat?
            ynovelo = ynovelo * -1;
        }

    }

    return 0;
}
