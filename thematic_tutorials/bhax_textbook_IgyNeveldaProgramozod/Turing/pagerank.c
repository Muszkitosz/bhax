#include <stdio.h>
#include <math.h>

void kiir (double tomb[], int db)
{
    for (int i=0; i<db; i++)
    {
        printf("%f\n",tomb[i]);   //a paraméterként kapott tömb elemeit,
                                  //azaz a PageRank értékeket kiíratja.
    }                             
}

int main()
{
    double L[4][4]={                       //linkmátrix                                         
        {0.0, 0.0, 1.0/3.0, 0.0},          //Minden oldalhoz tartozik 1 oszlop és egy sor.
        {1.0, 1.0/2.0, 1.0/3.0, 1.0},      //"i" sor és "j" oszlop esetén az elemeket úgy 
        {0.0, 1.0/2.0, 0.0, 0.0},          // kapjuk meg, hogy ha egy "j."oldal hivatkozik
        {0.0, 0.0, 1.0/3.0, 0.0}           // egy "i." oldalra, akkor az 1/(j-ről kimenő linkek
    };                                     // száma), minden más esetben 0.
                                                  

    double PR[4]={0.0, 0.0, 0.0, 0.0};     //PageRank értékek kiszámításához használt tömb.
                                         

    double PRv[4]={1.0/4.0, 1.0/4.0, 1.0/4.0, 1.0/4.0};    //Az értkékeknek összege mindig 1 	
							   // kell, hogy legyen, így minden(4db)
							   // oldalnak 0.25 jár.
                                 

    for (int i=0; i<100; i++)                                                           
    {                                      //azért van szükség egy külső ciklusra, mert minél
                                           // többször végezzük el a számítást, annál pontosabb
                                           // és pontosabb értkékeket fogunk kapni.
        for (int j=0; j<4; j++)            
        {                                  
        
            PR[j]=0.0;                     //A belső ciklus előtt minden sorhoz tartozó   
                                           // értkéket nullázunk, különben elég nagy számokat
                                           //kapnánk, hiszen mindig hozzáadjuk az előzőt is.
                                                                                                                                                                          
            for (int k=0; k<4; k++)        
            {    
                PR[j]=PR[j]+(L[j][k]*PRv[k]);    //a mátrix soraiból kapjuk meg
                         			 //a sorbeli oldal PG-jét.
                         //a linkmátrix ugyanolyan sorában minden oszlopbeli értéket
			 // megszorzunk a megfelelő PR-el.
            }                                               
        }        
        for (int l=0; l<4; l++)        
        {     
            PRv[l]=PR[l];         //eltároljuk a PR-t, így a következő futásnál új  
                          	  //értkékkel tudunk dolgozni.
        }                           
    }

    kiir(PR, 4);           //eredmény

    return 0;
}

