class Bird {
	public void makeSound() {
		System.out.println("Random bird sounds");
	}
}

class SingingBirds extends Bird {
	public void Sing () {
		System.out.println("Rhythmic bird sounds"); 	
	} //az énekes madarak tudnak énekelni
}

class Polimorph {
	public static void main(String[] args) {
		SingingBirds sbird1 = new SingingBirds();

		sbird1.makeSound();	//ezekkel nem lesz probléma, ugyanis a "sbird1"
		sbird1.Sing();		//referencia Gyermekosztály típusú, azaz megörökölte
					// a makeSound() metódust is

		Bird sbird2 = new SingingBirds();

		sbird2.makeSound();
		//sbird2.Sing();		//ezzel már probléma lesz, hiszen a "sbird2 Ősosztály
					// típusú referencia, tehát csak a makeSound()-ot érjük el
	}
}
