#include <iostream>

class Bird {
	public:
		void makeSound() {
			std::cout<<"Random bird sounds\n";
		}
};

class SingingBirds : public Bird {
	public:
		void Sing () { std::cout<<"Rhythmic bird sounds\n"; }
};

int main() {
	SingingBirds sbird1;
	
	sbird1.makeSound();
	sbird1.Sing();

	
	Bird sbird2;

	sbird2.makeSound();
	sbird2.Sing();
}

