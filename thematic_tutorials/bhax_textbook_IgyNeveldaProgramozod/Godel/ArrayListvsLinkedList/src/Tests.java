import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;

public class Tests {


    static void getTime(ArrayList c , int index){
        long startTime=System.nanoTime();
        c.get(index);
        long endTime=System.nanoTime();
        System.out.println("ArrayList \"get\" time: "+(endTime-startTime)+"ns");
    }
    static void getTime(LinkedList c , int index){
        long startTime=System.nanoTime();
        c.get(index);
        long endTime=System.nanoTime();
        System.out.println("LinkedList \"get\" time: "+(endTime-startTime)+"ns");
    }

    static void removeTime(ArrayList c , int index){
        long startTime=System.nanoTime();
        c.remove(index);
        long endTime=System.nanoTime();
        System.out.println("ArrayList \"remove\" time: "+(endTime-startTime)+"ns");
    }
    static void removeTime(LinkedList c , int index){
        long startTime=System.nanoTime();
        c.remove(index);
        long endTime=System.nanoTime();
        System.out.println("LinkedList \"remove\" time: "+(endTime-startTime)+"ns");
    }


}
