#include <stdlib.h>
#include <stdio.h>

max (int a, int b)
{
    return a > b ? a : b;
}

int main()
{    
    int a=5;
    int b=10;
    int maxi=max(a, b);
    printf("%d\n",maxi);
}