#include <stdio.h>
#include <stdlib.h>

int main()
{
    int n;
    printf("Az átváltandó decimális szám: ");
    scanf("%d",&n); //beolvassuk az átváltandó számot

    printf("Unáris számrendszerben: ");
    for (int i=0;i<n;i++)
    {
        printf("|");
    }
    printf("\n");
}